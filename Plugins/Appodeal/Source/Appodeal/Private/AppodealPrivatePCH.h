#pragma once

#include "CoreUObject.h"
#include "Engine.h"

#include "IAppodealModule.h"

DECLARE_LOG_CATEGORY_EXTERN(LogAppodeal, Log, All);

#if PLATFORM_IOS
#import <Appodeal/Appodeal.h>
#endif