#include "AppodealPrivatePCH.h"

#if PLATFORM_ANDROID
#include "AppodealJavaWrapper.h"
#endif

DEFINE_LOG_CATEGORY(LogAppodeal);

#define LOCTEXT_NAMESPACE "Appodeal"

class FAppodealModule : public IAppodealModule
{
	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

IMPLEMENT_MODULE(FAppodealModule, Appodeal)

void FAppodealModule::StartupModule() {

	#if PLATFORM_ANDROID
	   InitAppodealJavaMethods();
	#endif
}

void FAppodealModule::ShutdownModule() {
    // This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
    // we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
