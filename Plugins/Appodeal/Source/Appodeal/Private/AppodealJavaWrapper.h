#pragma once

#include "CoreMinimal.h"

#if PLATFORM_ANDROID
void InitAppodealJavaMethods();
void AndroidThunkCpp_AppodealInitialize(FString AppKey, bool INTERSTITIAL, bool REWARDED_VIDEO, bool BANNER);
void AndroidThunkCpp_AppodealSetAutoCache(uint8 AdType, bool Enable);
void AndroidThunkCpp_AppodealCache(uint8 AdType);

bool AndroidThunkCpp_AppodealIsLoaded(uint8 AdType);
bool AndroidThunkCpp_AppodealShow(uint8 AdType);

void AndroidThunkCpp_AppodealHideBanner();

void AndroidThunkCpp_AppodealSetTesting(bool Testing);
void AndroidThunkCpp_AppodealSetLogging(bool Logging);

void AndroidThunkCpp_AppodealDisableWriteExternalStoragePermissionCheck();
void AndroidThunkCpp_AppodealDisableLocationPermissionCheck();
void AndroidThunkCpp_AppodealDisableNetwork(FString Network, uint8 AdType);
void AndroidThunkCpp_AppodealDisableSmartBanners(bool Disable);
void AndroidThunkCpp_AppodealTrackInAppPurchase(float Amount, FString Name);
void AndroidThunkCpp_AppodealDisableBannerAnimation(bool flag);
void AndroidThunkCpp_AppodealDisable728x90Banners(bool flag);
#endif