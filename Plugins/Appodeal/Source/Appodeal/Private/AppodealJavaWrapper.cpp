#include "AppodealJavaWrapper.h"
#include "AppodealPrivatePCH.h"
#include "Appodeal.h"

#if PLATFORM_ANDROID
#include "Android/AndroidJNI.h"
#include "Android/AndroidApplication.h"
#endif

#if PLATFORM_ANDROID
static jmethodID AndroidThunkJava_AppodealInitialize;
static jmethodID AndroidThunkJava_ShowAppodealInterstitial;
static jmethodID AndroidThunkJava_AppodealShow;
static jmethodID AndroidThunkJava_AppodealSetAutoCache;
static jmethodID AndroidThunkJava_AppodealCache;
static jmethodID AndroidThunkJava_AppodealHideBanner;
static jmethodID AndroidThunkJava_AppodealIsLoaded;
static jmethodID AndroidThunkJava_AppodealConfirm;

static jmethodID AndroidThunkJava_AppodealSetTesting;
static jmethodID AndroidThunkJava_AppodealSetLogging;

static jmethodID AndroidThunkJava_AppodealDisableWriteExternalStoragePermissionCheck;
static jmethodID AndroidThunkJava_AppodealDisableLocationPermissionCheck;
static jmethodID AndroidThunkJava_AppodealDisableNetwork;
static jmethodID AndroidThunkJava_AppodealDisableSmartBanners;
static jmethodID AndroidThunkJava_AppodealTrackInAppPurchase;
static jmethodID AAndroidThunkJava_AppodealConfirm;
static jmethodID AndroidThunkJava_AppodealDisableBannerAnimation;
static jmethodID AndroidThunkJava_AppodealDisable728x90Banners;

void InitAppodealJavaMethods() {
UE_LOG(LogTemp, Log, TEXT("Appodeal: InitalizeMethods: Init"));
   if (JNIEnv* Env = FAndroidApplication::GetJavaEnv()) {
      if (FJavaWrapper::GameActivityClassID) {
			AndroidThunkJava_AppodealShow = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealShow", "(I)Z", false);
			AndroidThunkJava_AppodealInitialize = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealInitialize", "(Ljava/lang/String;ZZZ)V", true);
			AndroidThunkJava_AppodealSetAutoCache = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealSetAutoCache", "(IZ)V", true);
			AndroidThunkJava_AppodealCache = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealCache", "(I)V", true);
			AndroidThunkJava_AppodealHideBanner = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealHideBanner", "()V", true);
			AndroidThunkJava_AppodealIsLoaded = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealIsLoaded", "(I)Z", true);
			
			AndroidThunkJava_AppodealSetTesting = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealSetTesting", "(Z)V", true);
			AndroidThunkJava_AppodealSetLogging = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealSetLogging", "(Z)V", true);
			
			AndroidThunkJava_AppodealDisableWriteExternalStoragePermissionCheck = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealDisableWriteExternalStoragePermissionCheck", "()V", true);
			AndroidThunkJava_AppodealDisableLocationPermissionCheck = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealDisableLocationPermissionCheck", "()V", true);
			AndroidThunkJava_AppodealDisableNetwork = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealDisableNetwork", "(Ljava/lang/String;I)V", true);
			AndroidThunkJava_AppodealDisableSmartBanners = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealDisableSmartBanners", "(Z)V", true);
			AndroidThunkJava_AppodealTrackInAppPurchase = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealTrackInAppPurchase", "(FLjava/lang/String;)V", true);AndroidThunkJava_AppodealDisableBannerAnimation = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealDisableBannerAnimation", "(Z)V", true);AndroidThunkJava_AppodealDisable728x90Banners = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "AndroidThunkJava_AppodealDisable728x90Banners", "(Z)V", true);
      }
	  else UE_LOG(LogTemp, Log, TEXT("Appodeal: InitalizeMethods: No class"));
   }
   else UE_LOG(LogTemp, Log, TEXT("Appodeal: InitalizeMethods: No Env"));
}

//Initialize
void AndroidThunkCpp_AppodealInitialize(FString appKey, bool INTERSTITIAL, bool REWARDED_VIDEO, bool BANNER)
{
    if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
        if (AndroidThunkJava_AppodealInitialize)
        {
            jstring appKeyArg = Env->NewStringUTF(TCHAR_TO_UTF8(*appKey));
            FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealInitialize, appKeyArg, INTERSTITIAL, REWARDED_VIDEO, BANNER);
            Env->DeleteLocalRef(appKeyArg);

            UE_LOG(LogTemp, Log, TEXT("Appodeal: AppodealInitalize: Success Call"));
        }
        else UE_LOG(LogTemp, Log, TEXT("Appodeal: AppodealInitalize: No Method"));
    }
    else UE_LOG(LogTemp, Log, TEXT("Appodeal: AppodealInitalize: No Env"));
}

//Show
bool AndroidThunkCpp_AppodealShow(uint8 AdType)
{
    bool res = false;
    if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
        if (AndroidThunkJava_AppodealShow)
        {
            res = FJavaWrapper::CallBooleanMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealShow, AdType);
            UE_LOG(LogTemp, Log, TEXT("Appodeal: AppodealShow: Success Call"));
        }
        else UE_LOG(LogTemp, Log, TEXT("Appodeal: AppodealShow: No Method"));
    }
    else UE_LOG(LogTemp, Log, TEXT("Appodeal: AppodealShow: No Env"));
    return res;
}

//IsLoaded
bool AndroidThunkCpp_AppodealIsLoaded(uint8 AdType)
{
    bool res = false;
    if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
        if (AndroidThunkJava_AppodealIsLoaded)
        {
            res = FJavaWrapper::CallBooleanMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealIsLoaded, AdType);
            UE_LOG(LogTemp, Log, TEXT("Appodeal: AppodealIsLoaded: Success Call"));
        }
        else UE_LOG(LogTemp, Log, TEXT("Appodeal: AppodealIsLoaded: No Method"));
    }
    else UE_LOG(LogTemp, Log, TEXT("Appodeal: AppodealIsLoaded: No Env"));
    return res;
}

//SetAutoCache
void AndroidThunkCpp_AppodealSetAutoCache(uint8 AdType, bool Enable)
{
    if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
        if (AndroidThunkJava_AppodealSetAutoCache)
        {
            FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealSetAutoCache, AdType, Enable);
        }
    }
}

//Cache
void AndroidThunkCpp_AppodealCache(uint8 AdType)
{
    if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
        if (AndroidThunkJava_AppodealCache)
        {
            FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealCache, AdType);
        }
    }
}

//HideBanner
void AndroidThunkCpp_AppodealHideBanner()
{
    if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
        if (AndroidThunkJava_AppodealHideBanner)
        {
            FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealHideBanner);
        }
    }
}

//setTesting
void AndroidThunkCpp_AppodealSetTesting(bool Testing)
{
    if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
        if (AndroidThunkJava_AppodealSetTesting)
        {
            FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealSetTesting, Testing);
        }
    }
}

//setLogging
void AndroidThunkCpp_AppodealSetLogging(bool Logging)
{
    if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
        if (AndroidThunkJava_AppodealSetLogging)
        {
            FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealSetLogging, Logging);
        }
    }
}

//Disable Write External Storage Permission Check
void AndroidThunkCpp_AppodealDisableWriteExternalStoragePermissionCheck()
{
    if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
        if (AndroidThunkJava_AppodealDisableWriteExternalStoragePermissionCheck)
        {
            FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealDisableWriteExternalStoragePermissionCheck);
        }
    }
}

//Disable Location Permission Check
void AndroidThunkCpp_AppodealDisableLocationPermissionCheck()
{
    if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
        if (AndroidThunkJava_AppodealDisableLocationPermissionCheck)
        {
            FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealDisableLocationPermissionCheck);
        }
    }
}

//Disable Network
void AndroidThunkCpp_AppodealDisableNetwork(FString Network, uint8 AdType)
{
    if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
        if (AndroidThunkJava_AppodealDisableNetwork)
        {
            jstring networkArg = Env->NewStringUTF(TCHAR_TO_UTF8(*Network));
            FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealDisableNetwork, networkArg, AdType);
            Env->DeleteLocalRef(networkArg);
        }
    }
}

//setLogging
void AndroidThunkCpp_AppodealDisableSmartBanners(bool Value)
{
    if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
        if (AndroidThunkJava_AppodealDisableSmartBanners)
        {
            FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealDisableSmartBanners, Value);
        }
    }
}

//setLogging
void AndroidThunkCpp_AppodealTrackInAppPurchase(float Amount, FString Currency)
{
    if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
        if (AndroidThunkJava_AppodealTrackInAppPurchase)
        {
            jstring currencyArg = Env->NewStringUTF(TCHAR_TO_UTF8(*Currency));
            FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealTrackInAppPurchase, Amount, currencyArg);
            Env->DeleteLocalRef(currencyArg);
        }
    }
}

void AndroidThunkCpp_AppodealDisableBannerAnimation(bool flag){
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
		if(AndroidThunkJava_AppodealDisableBannerAnimation){
			FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealDisableBannerAnimation, flag);
		}
	}
}

void AndroidThunkCpp_AppodealDisable728x90Banners(bool flag){
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
    {
		if(AndroidThunkJava_AppodealDisable728x90Banners){
			FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GoogleServicesThis, AndroidThunkJava_AppodealDisable728x90Banners, flag);
		}
	}
}

                            /* ------------ Callbacks ------------ */


                                    /* ------ Banner ------ */
//This functions is declared in the Java-defined class, GameActivity.java"
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnBannerLoaded(JNIEnv* jenv, jobject thiz, jint height, jboolean isPrecache)
{
    UAppodeal::OnBannerLoaded_Broadcast(height, isPrecache);
}
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnBannerFailedToLoad(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnBannerFailedToLoad_Broadcast();
}

extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnBannerShown(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnBannerShown_Broadcast();
}
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnBannerClicked(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnBannerClicked_Broadcast();
}
    
                                    /* ------ Interstitial ------ */
//This functions is declared in the Java-defined class, GameActivity.java"
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnInterstitialLoaded(JNIEnv* jenv, jobject thiz, jboolean isPrecache)
{
	UAppodeal::OnInterstitialLoaded_Broadcast(isPrecache);
}
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnInterstitialFailedToLoad(JNIEnv* jenv)
{
	UAppodeal::OnInterstitialFailedToLoad_Broadcast();
}

extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnInterstitialShown(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnInterstitialShown_Broadcast();
}
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnInterstitialClicked(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnInterstitialClicked_Broadcast();
} 
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnInterstitialClosed(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnInterstitialClosed_Broadcast();
} 

                                    /* ------ Skippable Video ------ */
//This functions is declared in the Java-defined class, GameActivity.java"
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnSkippableVideoLoaded(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnSkippableVideoLoaded_Broadcast();
}
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnSkippableVideoFailedToLoad(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnSkippableVideoFailedToLoad_Broadcast();
}

extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnSkippableVideoShown(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnSkippableVideoShown_Broadcast();
}
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnSkippableVideoFinished(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnSkippableVideoFinished_Broadcast();
} 
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnSkippableVideoClosed(JNIEnv* jenv, jobject thiz, jboolean isFinished)
{
	UAppodeal::OnSkippableVideoClosed_Broadcast(isFinished);
}    

                                    /* ------ Non OnNonSkippable Video ------ */
//This functions is declared in the Java-defined class, GameActivity.java"
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnNonSkippableVideoLoaded(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnNonSkippableVideoLoaded_Broadcast();
}
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnNonSkippableVideoFailedToLoad(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnNonSkippableVideoFailedToLoad_Broadcast();
}

extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnNonSkippableVideoShown(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnNonSkippableVideoShown_Broadcast();
}
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnNonSkippableVideoFinished(JNIEnv* jenv, jobject thizv)
{
	UAppodeal::OnNonSkippableVideoFinished_Broadcast();
} 
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnNonSkippableVideoClosed(JNIEnv* jenv, jobject thiz, jboolean isFinished)
{
	UAppodeal::OnNonSkippableVideoClosed_Broadcast(isFinished);
}  

                                    /* ------ Rewarded Video ------ */
//This functions is declared in the Java-defined class, GameActivity.java"
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnRewardedVideoLoaded(JNIEnv* jenv, jobject thiz, jboolean isPrecache)
{
	UAppodeal::OnRewardedVideoLoaded_Broadcast(isPrecache);
}
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnRewardedVideoFailedToLoad(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnRewardedVideoFailedToLoad_Broadcast();
}

extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnRewardedVideoShown(JNIEnv* jenv, jobject thiz)
{
	UAppodeal::OnRewardedVideoShown_Broadcast();
}
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnRewardedVideoFinished(JNIEnv* jenv, jobject thiz, jdouble amount, jstring name)
{
    const char* JavaNameString = jenv->GetStringUTFChars(name, 0);
	UAppodeal::OnRewardedVideoFinished_Broadcast(amount, JavaNameString);
    jenv->ReleaseStringUTFChars(name, JavaNameString);
} 
extern "C" void Java_com_epicgames_ue4_GameActivity_nativeOnRewardedVideoClosed(JNIEnv* jenv, jobject thiz, jboolean isFinished)
{
	UAppodeal::OnRewardedVideoClosed_Broadcast(isFinished);
}   

#endif