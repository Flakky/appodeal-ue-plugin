#include "Appodeal.h"
#include "AppodealPrivatePCH.h"

#if PLATFORM_ANDROID
#include "AppodealJavaWrapper.h"
#endif

UAppodeal* UAppodeal::AppodealObj = nullptr;

UAppodeal::UAppodeal() {
	AppodealObj = this;
}
UAppodeal::~UAppodeal() {
	AppodealObj = nullptr; 
}

#if PLATFORM_IOS
@interface AppodealDelegate: NSObject <AppodealBannerDelegate, AppodealNonSkippableVideoDelegate, AppodealSkippableVideoDelegate, AppodealInterstitialDelegate, AppodealRewardedVideoDelegate>

@end

static AppodealDelegate *AppodealDelegateInstance;
#endif

void UAppodeal::AppodealInitialize() {
    #if PLATFORM_ANDROID
		AppodealSetTesting(enableTesting);
		AppodealSetLogging(enableLogging);
		AppodealDisableSmartBanners(disableSmartBanners);
		AppodealDisableBannerAnimation(disableBannerAnimation);
		AppodealDisable728x90Banners(disable728x90Banners);
		
		if(disableLocation) AppodealDisableLocationPermissionCheck();
		if(disableWriteExternalStorage) AppodealDisableWriteExternalStoragePermissionCheck();
		
		AppodealInitializeWithParameters(androidAppKey, iosAppKey, initializeInterstitial, intializeRewardedVideo, initializeBanner);
    #elif PLATFORM_IOS
		AppodealConfirm(confirmSkippable);
		if(disableLocation) AppodealDisableLocationPermissionCheck();
		
        AppodealInitializeWithParameters(androidAppKey, iosAppKey, initializeInterstitial, initializeSkippableVideo, initializeNonSkippableVideo, intializeRewardedVideo, initializeBanner);
		
		AppodealDisableAutoCache(disableAutoCacheInterstitial, disableAutoCacheSkippableVideo, disableAutoCacheNonSkippableVideo, disableAutoCacheRewardedVideo, disableAutoCacheBanner);
		AppodealSetTesting(enableTesting);
		AppodealSetLogging(enableLogging);
		AppodealDisableSmartBanners(disableSmartBanners);
		AppodealDisableBannerAnimation(disableBannerAnimation);
		AppodealDisable728x90Banners(disable728x90Banners);
    #endif
}

void UAppodeal::AppodealInitializeWithParameters(FString androidAppKey, FString iosAppKey, bool INTERSTITIAL, bool REWARDED_VIDEO, bool BANNER) {
    #if PLATFORM_ANDROID
        AndroidThunkCpp_AppodealInitialize(androidAppKey, INTERSTITIAL, REWARDED_VIDEO, BANNER);
    #elif PLATFORM_IOS
        dispatch_async(dispatch_get_main_queue(), ^{
            AppodealDelegateInstance = [AppodealDelegate new];
            AppodealAdType adType = 0;
            if(INTERSTITIAL) {
                adType = AppodealAdTypeInterstitial;
                [Appodeal setInterstitialDelegate:AppodealDelegateInstance];
            }
            if (SKIPPABLE_VIDEO) {
                adType = adType | AppodealAdTypeSkippableVideo;
                [Appodeal setSkippableVideoDelegate:AppodealDelegateInstance];
            }
            if (NON_SKIPPABLE_VIDEO) {
                adType = adType | AppodealAdTypeNonSkippableVideo;
                [Appodeal setNonSkippableVideoDelegate:AppodealDelegateInstance];
            }
            if (REWARDED_VIDEO) {
                adType = adType | AppodealAdTypeRewardedVideo;
                [Appodeal setRewardedVideoDelegate:AppodealDelegateInstance];
            }
            if (BANNER) {
                adType = adType | AppodealAdTypeBanner;
                [Appodeal setBannerDelegate:AppodealDelegateInstance];
            }
            [Appodeal disableLocationPermissionCheck];
            [Appodeal initializeWithApiKey:iosAppKey.GetNSString() types:adType];
        });
    #endif
}

bool UAppodeal::AppodealShow(EAdType AdType)
{
    #if PLATFORM_ANDROID
	switch (AdType) {
		case EAdType::EAT_INTERSTITIAL:
			return AndroidThunkCpp_AppodealShow(0);
		case EAdType::EAT_REWARDED_VIDEO:
			return AndroidThunkCpp_AppodealShow(1);
		case EAdType::EAT_BANNER:
			return AndroidThunkCpp_AppodealShow(2);
	}     
    #elif PLATFORM_IOS
        __block BOOL show = false;
        dispatch_async(dispatch_get_main_queue(), ^{
            AppodealShowStyle adType;
            if(INTERSTITIAL) {
                adType = AppodealShowStyleInterstitial;
            }
            if (SKIPPABLE_VIDEO) {
                adType = AppodealShowStyleSkippableVideo;
            }
            if (NON_SKIPPABLE_VIDEO) {
                adType = AppodealShowStyleNonSkippableVideo;
            }
            if (REWARDED_VIDEO) {
                adType = AppodealShowStyleRewardedVideo;
            }
            if (BANNER_BOTTOM) {
                adType = AppodealShowStyleBannerBottom;
            }
            if (BANNER_TOP) {
                adType = AppodealShowStyleBannerTop;
            }
            if (INTERSTITIAL && SKIPPABLE_VIDEO) {
                adType = AppodealShowStyleVideoOrInterstitial;
            }
            show = [Appodeal showAd:adType rootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
        });
        return show;
    #else
        return false;
    #endif
}

bool UAppodeal::AppodealIsLoaded(EAdType AdType)
{
    #if PLATFORM_ANDROID
		return AndroidThunkCpp_AppodealShow((uint8)AdType);
    #elif PLATFORM_IOS
        __block BOOL isLoaded = false;
        dispatch_async(dispatch_get_main_queue(), ^{
            AppodealShowStyle adType;
            if(INTERSTITIAL) {
                adType = AppodealShowStyleInterstitial;
            }
            if (SKIPPABLE_VIDEO) {
                adType = AppodealShowStyleSkippableVideo;
            }
            if (NON_SKIPPABLE_VIDEO) {
                adType = AppodealShowStyleNonSkippableVideo;
            }
            if (REWARDED_VIDEO) {
                adType = AppodealShowStyleRewardedVideo;
            }
            if (BANNER) {
                adType = AppodealShowStyleBannerBottom;
            }
            if (INTERSTITIAL && SKIPPABLE_VIDEO) {
                adType = AppodealShowStyleVideoOrInterstitial;
            }
            isLoaded = [Appodeal isReadyForShowWithStyle:adType]; 
        });  
        return isLoaded;
    #else
        return false;
    #endif
}

void UAppodeal::AppodealSetAutoCache(EAdType AdType, bool Enable)
{
    #if PLATFORM_ANDROID
        AndroidThunkCpp_AppodealSetAutoCache((uint8)AdType, Enable);
    #elif PLATFORM_IOS
        dispatch_async(dispatch_get_main_queue(), ^{
            AppodealAdType adType = 0;
            if(INTERSTITIAL) {
                [Appodeal setAutocache:false types:AppodealAdTypeInterstitial];
            }
            if (SKIPPABLE_VIDEO) {
                [Appodeal setAutocache:false types:AppodealAdTypeSkippableVideo];
            }
            if (NON_SKIPPABLE_VIDEO) {
                [Appodeal setAutocache:false types:AppodealAdTypeNonSkippableVideo];
            }
            if (REWARDED_VIDEO) {
                [Appodeal setAutocache:false types:AppodealAdTypeRewardedVideo];
            }
            if (BANNER) {
                [Appodeal setAutocache:false types:AppodealAdTypeBanner];
            }
        });
    #endif
}

void UAppodeal::AppodealCache(EAdType AdType)
{
    #if PLATFORM_ANDROID
        AndroidThunkCpp_AppodealCache((uint8)AdType);
    #elif PLATFORM_IOS
        dispatch_async(dispatch_get_main_queue(), ^{
            if(INTERSTITIAL) {
                [Appodeal cacheAd:AppodealAdTypeInterstitial];
            }
            if (SKIPPABLE_VIDEO) {
               [Appodeal cacheAd:AppodealAdTypeSkippableVideo];
            }
            if (NON_SKIPPABLE_VIDEO) {
                [Appodeal cacheAd:AppodealAdTypeNonSkippableVideo];
            }
            if (REWARDED_VIDEO) {
                [Appodeal cacheAd:AppodealAdTypeRewardedVideo];
            }
            if (BANNER) {
                [Appodeal cacheAd:AppodealAdTypeBanner];
            }
        });
    #endif
}

void UAppodeal::AppodealSetTesting(bool Testing)
{
    #if PLATFORM_ANDROID
        AndroidThunkCpp_AppodealSetTesting(Testing);
    #elif PLATFORM_IOS
        dispatch_async(dispatch_get_main_queue(), ^{
            [Appodeal setTestingEnabled:Testing];
        });
    #endif
}

void UAppodeal::AppodealSetLogging(bool Logging)
{
    #if PLATFORM_ANDROID
        AndroidThunkCpp_AppodealSetLogging(Logging);
    #endif
}

void UAppodeal::AppodealHideBanner()
{
    #if PLATFORM_ANDROID
        AndroidThunkCpp_AppodealHideBanner();
    #elif PLATFORM_IOS
        dispatch_async(dispatch_get_main_queue(), ^{
            [Appodeal hideBanner];
        });
    #endif
}

void UAppodeal::AppodealDisableWriteExternalStoragePermissionCheck()
{
    #if PLATFORM_ANDROID
        AndroidThunkCpp_AppodealDisableWriteExternalStoragePermissionCheck();
    #endif
}

void UAppodeal::AppodealDisableLocationPermissionCheck()
{
    #if PLATFORM_ANDROID
        AndroidThunkCpp_AppodealDisableLocationPermissionCheck();
    #elif PLATFORM_IOS
        dispatch_async(dispatch_get_main_queue(), ^{
            [Appodeal disableLocationPermissionCheck];
        });
    #endif
}

void UAppodeal::AppodealDisableNetwork(FString Network, EAdType AdType)
{
    #if PLATFORM_ANDROID
        AndroidThunkCpp_AppodealDisableNetwork(Network, (uint8)AdType);
    #elif PLATFORM_IOS
        dispatch_async(dispatch_get_main_queue(), ^{
            if(INTERSTITIAL) {
                [Appodeal disableNetworkForAdType:AppodealAdTypeInterstitial name:Network.GetNSString()];
            }
            if (SKIPPABLE_VIDEO) {
                [Appodeal disableNetworkForAdType:AppodealAdTypeSkippableVideo name:Network.GetNSString()];
            }
            if (NON_SKIPPABLE_VIDEO) {
                [Appodeal disableNetworkForAdType:AppodealAdTypeNonSkippableVideo name:Network.GetNSString()];
            }
            if (REWARDED_VIDEO) {
                [Appodeal disableNetworkForAdType:AppodealAdTypeRewardedVideo name:Network.GetNSString()];
            }
            if (BANNER) {
                [Appodeal disableNetworkForAdType:AppodealAdTypeBanner name:Network.GetNSString()];
            }
        });
    #endif
}

void UAppodeal::AppodealDisableSmartBanners(bool Value)
{
    #if PLATFORM_ANDROID
        AndroidThunkCpp_AppodealDisableSmartBanners(Value);
    #elif PLATFORM_IOS 
        [Appodeal setSmartBannersEnabled:Value];
    #endif
}

void UAppodeal::AppodealTrackInAppPurchase(float Amount, FString Currency)
{
    #if PLATFORM_ANDROID
        AndroidThunkCpp_AppodealTrackInAppPurchase(Amount, Currency);
    #elif PLATFORM_IOS 
        [[APDSdk sharedSdk] trackInAppPurchase:[NSNumber numberWithInt:Amount] currency:Currency.GetNSString()];
    #endif
}

void UAppodeal::AppodealDisableBannerAnimation(bool Disable){
	#if PLATFORM_ANDROID
		AndroidThunkCpp_AppodealDisableBannerAnimation(Disable);
	#elif PLATFORM_IOS
        [Appodeal setBannerAnimationEnabled:Disable];
	#endif
}

void UAppodeal::AppodealDisable728x90Banners(bool Disable){
	#if PLATFORM_ANDROID
		AndroidThunkCpp_AppodealDisable728x90Banners(Disable);
	#endif
}



#if PLATFORM_IOS
@implementation AppodealDelegate

- (void)bannerDidLoadAdisPrecache:(BOOL)precache {
    UAppodeal::OnBannerLoaded_Broadcast(0, precache);
}

- (void)bannerDidFailToLoadAd {
    UAppodeal::OnBannerFailedToLoad_Broadcast();
}

- (void)bannerDidClick {
    UAppodeal::OnBannerClicked_Broadcast();
}

- (void)bannerDidShow {
    UAppodeal::OnBannerShown_Broadcast();
}


- (void)interstitialDidLoadAdisPrecache:(BOOL)precache {
    UAppodeal::OnInterstitialLoaded_Broadcast(precache);
}

- (void)interstitialDidFailToLoadAd {
    UAppodeal::OnInterstitialFailedToLoad_Broadcast();
}

- (void)interstitialWillPresent {
    UAppodeal::OnInterstitialShown_Broadcast();
}

- (void)interstitialDidClick {
    UAppodeal::OnInterstitialClicked_Broadcast();
}

- (void)interstitialDidDismiss {
    UAppodeal::OnInterstitialClosed_Broadcast();
}



- (void)rewardedVideoDidLoadAd {
    UAppodeal::OnRewardedVideoLoaded_Broadcast();
    
}

- (void)rewardedVideoDidFailToLoadAd {
    UAppodeal::OnRewardedVideoFailedToLoad_Broadcast();
    
}

- (void)rewardedVideoDidPresent {
    UAppodeal::OnRewardedVideoShown_Broadcast();
    
}

- (void)rewardedVideoWillDismiss {
    UAppodeal::OnRewardedVideoClosed_Broadcast(false);
    
}

- (void)rewardedVideoDidFinish:(NSUInteger)rewardAmount name:(NSString *)rewardName {
    UAppodeal::OnRewardedVideoFinished_Broadcast(rewardAmount, FString(rewardName));
    
}


- (void)nonSkippableVideoDidLoadAd {
    UAppodeal::OnNonSkippableVideoLoaded_Broadcast();
}

- (void) nonSkippableVideoDidFailToLoadAd {
    UAppodeal::OnNonSkippableVideoFailedToLoad_Broadcast();
}

- (void)nonSkippableVideoDidClick { }

- (void)nonSkippableVideoDidFinish {
    UAppodeal::OnNonSkippableVideoFinished_Broadcast();
}

- (void)nonSkippableVideoDidPresent {
    UAppodeal::OnNonSkippableVideoShown_Broadcast();
}

- (void)nonSkippableVideoWillDismiss {
    UAppodeal::OnNonSkippableVideoClosed_Broadcast(false);
}


- (void)skippableVideoDidLoadAd {
    UAppodeal::OnSkippableVideoLoaded_Broadcast();
}

- (void)skippableVideoDidFailToLoadAd {
    UAppodeal::OnSkippableVideoFailedToLoad_Broadcast();
}

- (void)skippableVideoDidClick { }

- (void)skippableVideoDidPresent {
    UAppodeal::OnSkippableVideoShown_Broadcast();
}

- (void)skippableVideoWillDismiss {
    UAppodeal::OnSkippableVideoFinished_Broadcast();
}

- (void)skippableVideoDidFinish {
    UAppodeal::OnSkippableVideoClosed_Broadcast(false);
}

@end

#endif
