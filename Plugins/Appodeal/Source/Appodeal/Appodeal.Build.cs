using UnrealBuildTool;
using System.IO;
using System;

public class Appodeal : ModuleRules
{
    public Appodeal(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.AddRange(
            new string[] {
                // ... add public include paths required here ...
            }
        );
        
        PrivateIncludePaths.AddRange(
            new string[] {
                "Appodeal/Private",
                // ... add other private include paths required here ...
            }
        );
        
        PublicDependencyModuleNames.AddRange(
            new string[] {
                // ... add other public dependencies that you statically link with here ...
            }
        );
        
        PrivateDependencyModuleNames.AddRange(
            new string[] {
                "Core",
                "CoreUObject",
                "Engine"
                // ... add private dependencies that you statically link with here ...
            }
        );
        
        DynamicallyLoadedModuleNames.AddRange(
            new string[] {
                // ... add any modules that your module loads dynamically here ...
            }
        );
        
        if (Target.Platform == UnrealTargetPlatform.IOS)
        {
            
            PublicAdditionalFrameworks.Add(
                new UEBuildFramework(
                    "Appodeal",
                    "../../ThirdPartyLibraries/iOS/Appodeal.zip",
                    "Resources/"
                )
            );
            
            PublicFrameworks.AddRange(
                new string[] {
                    "AdSupport",
                    "AudioToolbox",
                    "AVFoundation",
                    "CFNetwork",
                    "CoreFoundation",
                    "CoreGraphics",
                    "CoreImage",
                    "CoreLocation",
                    "CoreMedia",
                    "CoreMotion",
                    "CoreTelephony",
                    "CoreBluetooth",
                    "EventKit",
                    "EventKitUI",
                    "MediaPlayer",
                    "MessageUI",
                    "MobileCoreServices",
                    "QuartzCore",
                    "Security",
                    "Social",
                    "StoreKit",
                    "SystemConfiguration",
                    "SafariServices",
                    "Twitter",
                    "UIKit",
                    "WebKit",
                    "GLKit",
                    "JavaScriptCore"
                }
            );
            
            PublicAdditionalLibraries.Add("c++");
            PublicAdditionalLibraries.Add("sqlite3");
            PublicAdditionalLibraries.Add("xml2.2");
            PublicAdditionalLibraries.Add("z");

            PrivateDependencyModuleNames.AddRange(
                new string[] {
                    "Launch"
                }
            );
            

            AdditionalPropertiesForReceipt.Add(new ReceiptProperty("IOSPlugin", Path.Combine(ModuleDirectory, "Appodeal_IPL.xml")));
            
        }

        if (Target.Platform == UnrealTargetPlatform.Android) {
            PrivateDependencyModuleNames.Add("Launch");

            AdditionalPropertiesForReceipt.Add("AndroidPlugin", Path.Combine(ModuleDirectory, "Appodeal_APL.xml"));
        }
    }
}