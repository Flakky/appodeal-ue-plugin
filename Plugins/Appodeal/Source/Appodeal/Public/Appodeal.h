#pragma once


#include "Appodeal.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FBannerLoaded, int, height, bool, isPrecache);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBannerFailedToLoad);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBannerShown);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBannerClicked);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInterstitialLoaded, bool, isPrecache);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInterstitialFailedToLoad);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInterstitialShown);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInterstitialClicked);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInterstitialClosed);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSkippableVideoLoaded);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSkippableVideoFailedToLoad);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSkippableVideoShown);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSkippableVideoFinished);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSkippableVideoClosed, bool, isFinished);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNonSkippableVideoLoaded);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNonSkippableVideoFailedToLoad);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNonSkippableVideoShown);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNonSkippableVideoFinished);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FNonSkippableVideoClosed, bool, isFinished);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRewardedVideoLoaded, bool, isPrecache);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRewardedVideoFailedToLoad);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRewardedVideoShown);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FRewardedVideoFinished, double, amount, FString, name);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRewardedVideoClosed, bool, isFinished);

UENUM(BlueprintType)
enum class EAdType : uint8
{
	EAT_INTERSTITIAL        UMETA(DisplayName = "Interstitial"),
	EAT_REWARDED_VIDEO      UMETA(DisplayName = "Rewarded Video"),
	EAT_BANNER              UMETA(DisplayName = "Banner")
};

UCLASS(ClassGroup = Advertising, Blueprintable)
class UAppodeal : public UObject
{
   GENERATED_BODY()

private:

	static UAppodeal* AppodealObj;

public:
	
	UAppodeal();
	~UAppodeal();
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	void				AppodealInitialize();
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static void			AppodealInitializeWithParameters		(FString androidAppKey, FString iosAppKey, bool INTERSTITIAL, bool REWARDED_VIDEO, bool BANNER);
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static bool			AppodealShow							(EAdType AdType);
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static void			AppodealCache							(EAdType AdType);
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static void			AppodealHideBanner						();
	
	UFUNCTION(BlueprintPure, Category = "Advertising|Appodeal")
	static bool			AppodealIsLoaded						(EAdType AdType);
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static void			AppodealDisableNetwork					(FString Network, EAdType AdType);
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static void			AppodealTrackInAppPurchase				(float amount, FString currency);
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static void			AppodealSetAutoCache					(EAdType AdType, bool Enable);
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static void			AppodealSetTesting						(bool Testing);
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static void			AppodealSetLogging						(bool Logging);
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static void			AppodealDisableSmartBanners				(bool Disable);
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static void			AppodealDisableBannerAnimation			(bool Disable);
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static void			AppodealDisable728x90Banners			(bool Disable);
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static void			AppodealDisableWriteExternalStoragePermissionCheck();
	
	UFUNCTION(BlueprintCallable, Category = "Advertising|Appodeal")
	static void			AppodealDisableLocationPermissionCheck	();
	
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Android application key"))
	FString				androidAppKey;
	UPROPERTY(EditAnywhere, meta = (DisplayName = "iOS application key"))
	FString				iosAppKey;
	
	UPROPERTY(EditAnywhere, Category="Ad Types To Initialize", meta = (DisplayName = "Interstitial"))
	bool				initializeInterstitial;
	UPROPERTY(EditAnywhere, Category="Ad Types To Initialize", meta = (DisplayName = "Skippable video"))
	bool				initializeSkippableVideo; 
	UPROPERTY(EditAnywhere, Category="Ad Types To Initialize", meta = (DisplayName = "Non skippable video"))
	bool				initializeNonSkippableVideo;
	UPROPERTY(EditAnywhere, Category="Ad Types To Initialize", meta = (DisplayName = "Rewarded video"))
	bool				intializeRewardedVideo;
	UPROPERTY(EditAnywhere, Category="Ad Types To Initialize", meta = (DisplayName = "Banner"))
	bool				initializeBanner;
	UPROPERTY(EditAnywhere, Category="Ad Types To Initialize", meta = (DisplayName = "Confirm skippable video"))
	bool				confirmSkippable;
	
	UPROPERTY(EditAnywhere, Category="Disable autocache", meta = (DisplayName = "Interstitial"))
	bool				disableAutoCacheInterstitial;
	UPROPERTY(EditAnywhere, Category="Disable autocache", meta = (DisplayName = "Skippable video"))
	bool				disableAutoCacheSkippableVideo; 
	UPROPERTY(EditAnywhere, Category="Disable autocache", meta = (DisplayName = "Non skippable video"))
	bool				disableAutoCacheNonSkippableVideo;
	UPROPERTY(EditAnywhere, Category="Disable autocache", meta = (DisplayName = "Rewarded video"))
	bool				disableAutoCacheRewardedVideo;
	UPROPERTY(EditAnywhere, Category="Disable autocache", meta = (DisplayName = "Banner"))
	bool				disableAutoCacheBanner;
	
	UPROPERTY(EditAnywhere, Category="Additional settings", meta = (DisplayName = "Testing"))
	bool				enableTesting;
	UPROPERTY(EditAnywhere, Category="Additional settings", meta = (DisplayName = "Logging"))
	bool				enableLogging;
	UPROPERTY(EditAnywhere, Category="Additional settings", meta = (DisplayName = "Disable banner animation"))
	bool				disableBannerAnimation;
	UPROPERTY(EditAnywhere, Category="Additional settings", meta = (DisplayName = "Disable smart banners"))
	bool				disableSmartBanners;
	UPROPERTY(EditAnywhere, Category="Additional settings", meta = (DisplayName = "Disable 728x90 banners"))
	bool				disable728x90Banners;
	
	UPROPERTY(EditAnywhere, Category="Additional settings", meta = (DisplayName = "Disable location permission check"))
	bool				disableLocation;
	UPROPERTY(EditAnywhere, Category="Additional settings", meta = (DisplayName = "Disable write external storage check"))
	bool				disableWriteExternalStorage;
	
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FBannerLoaded		OnBannerLoaded;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FBannerFailedToLoad	OnBannerFailedToLoad;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FBannerShown		OnBannerShown;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FBannerClicked		OnBannerClicked;
	
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FInterstitialLoaded OnInterstitialLoaded;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FInterstitialFailedToLoad OnInterstitialFailedToLoad;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FInterstitialShown OnInterstitialShown;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FInterstitialClicked OnInterstitialClicked;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FInterstitialClosed OnInterstitialClosed;
	
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FSkippableVideoLoaded OnSkippableVideoLoaded;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FSkippableVideoFailedToLoad OnSkippableVideoFailedToLoad;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FSkippableVideoShown OnSkippableVideoShown;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FSkippableVideoFinished OnSkippableVideoFinished;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FSkippableVideoClosed OnSkippableVideoClosed;
	
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FNonSkippableVideoLoaded OnNonSkippableVideoLoaded;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FNonSkippableVideoFailedToLoad OnNonSkippableVideoFailedToLoad;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FNonSkippableVideoShown OnNonSkippableVideoShown;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FNonSkippableVideoFinished OnNonSkippableVideoFinished;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FNonSkippableVideoClosed OnNonSkippableVideoClosed;
	
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FRewardedVideoLoaded OnRewardedVideoLoaded;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FRewardedVideoFailedToLoad OnRewardedVideoFailedToLoad;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FRewardedVideoShown OnRewardedVideoShown;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FRewardedVideoFinished OnRewardedVideoFinished;
	UPROPERTY(BlueprintAssignable, Category = "Advertising|Appodeal")
	FRewardedVideoClosed OnRewardedVideoClosed;
	
	static void OnBannerLoaded_Broadcast(int height, bool isPrecache) { AppodealObj->OnBannerLoaded.Broadcast(height, isPrecache); }
	static void OnBannerFailedToLoad_Broadcast() { AppodealObj->OnBannerFailedToLoad.Broadcast(); }
	static void OnBannerShown_Broadcast() { AppodealObj->OnBannerShown.Broadcast(); }
	static void OnBannerClicked_Broadcast() { AppodealObj->OnBannerClicked.Broadcast(); }
	
	static void OnInterstitialLoaded_Broadcast(bool isPrecache) { AppodealObj->OnInterstitialLoaded.Broadcast(isPrecache); }
	static void OnInterstitialFailedToLoad_Broadcast() { AppodealObj->OnInterstitialFailedToLoad.Broadcast(); }
	static void OnInterstitialShown_Broadcast() { AppodealObj->OnInterstitialShown.Broadcast(); }
	static void OnInterstitialClicked_Broadcast() { AppodealObj->OnInterstitialClicked.Broadcast(); }
	static void OnInterstitialClosed_Broadcast() { AppodealObj->OnInterstitialClosed.Broadcast(); }
	
	static void OnSkippableVideoLoaded_Broadcast() { AppodealObj->OnSkippableVideoLoaded.Broadcast(); }
	static void OnSkippableVideoFailedToLoad_Broadcast() { AppodealObj->OnSkippableVideoFailedToLoad.Broadcast(); }
	static void OnSkippableVideoShown_Broadcast() { AppodealObj->OnSkippableVideoShown.Broadcast(); }
	static void OnSkippableVideoFinished_Broadcast() { AppodealObj->OnSkippableVideoFinished.Broadcast(); }
	static void OnSkippableVideoClosed_Broadcast(bool isFinished) { AppodealObj->OnSkippableVideoClosed.Broadcast(isFinished); }
	
	static void OnNonSkippableVideoLoaded_Broadcast() { AppodealObj->OnNonSkippableVideoLoaded.Broadcast(); }
	static void OnNonSkippableVideoFailedToLoad_Broadcast() { AppodealObj->OnNonSkippableVideoFailedToLoad.Broadcast(); }
	static void OnNonSkippableVideoShown_Broadcast() { AppodealObj->OnNonSkippableVideoShown.Broadcast(); }
	static void OnNonSkippableVideoFinished_Broadcast() { AppodealObj->OnNonSkippableVideoFinished.Broadcast(); }
	static void OnNonSkippableVideoClosed_Broadcast(bool isFinished) { AppodealObj->OnNonSkippableVideoClosed.Broadcast(isFinished); }
	
	static void OnRewardedVideoLoaded_Broadcast(bool isPrecache) { AppodealObj->OnRewardedVideoLoaded.Broadcast(isPrecache); }
	static void OnRewardedVideoFailedToLoad_Broadcast() { AppodealObj->OnRewardedVideoFailedToLoad.Broadcast(); }
	static void OnRewardedVideoShown_Broadcast() { AppodealObj->OnRewardedVideoShown.Broadcast(); }
	static void OnRewardedVideoFinished_Broadcast(double amount, FString name) { AppodealObj->OnRewardedVideoFinished.Broadcast(amount, name); }
	static void OnRewardedVideoClosed_Broadcast(bool isFinished) { AppodealObj->OnRewardedVideoClosed.Broadcast(isFinished); }

};