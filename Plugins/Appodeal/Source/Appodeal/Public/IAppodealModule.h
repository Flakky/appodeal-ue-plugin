// Copyright 2014-1016, 2Scars Games. All Rights Reserved.
#pragma once
#include "ModuleManager.h"


/**
 * The public interface to this module.  In most cases, this interface is only public to sibling modules 
 * within this plugin.
 */
class IAppodealModule : public IModuleInterface
{

public:

	/**
	 * Singleton-like access to this module's interface.  This is just for convenience!
	 * Beware of calling this during the shutdown phase, though.  Your module might have been unloaded already.
	 *
	 * @return Returns singleton instance, loading the module on demand if needed
	 */
	static inline IAppodealModule& Get()
	{
		return FModuleManager::LoadModuleChecked< IAppodealModule >( "Appodeal" );
	}

};

